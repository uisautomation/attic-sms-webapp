Google Cloud Deployment
=======================

Getting started
---------------

The `running Django on GCE
<https://cloud.google.com/python/django/container-engine>`_ tutorial forms the
base of the Google Cloud deployment.

.. note::

    This section will be completed as part of another story.
