Streaming Media Service Webapp
==============================

The University of Cambridge Streaming Media Service provides a place for members
of the University of Cambridge to host video and audio content. This is the
developer documentation for the web control panel.

.. toctree::
    :maxdepth: 2
    :caption: Contents

    gettingstarted
    developer
    deployment
    configuration
    smswebapp
