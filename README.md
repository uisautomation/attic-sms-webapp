# Streaming Media Service Webapp

[![Documentation
Status](https://readthedocs.org/projects/uis-smswebapp/badge/?version=latest)](http://uis-smswebapp.readthedocs.io/en/latest/?badge=latest)
[![CircleCI](https://circleci.com/bb/uisautomation/sms-webapp.svg?style=svg)](https://circleci.com/bb/uisautomation/sms-webapp)
[![Coverage
Status](https://coveralls.io/repos/bitbucket/uisautomation/sms-webapp/badge.svg)](https://coveralls.io/bitbucket/uisautomation/sms-webapp)

Documentation for developers, including a "getting started" guide, can be found
at https://uis-smswebapp.readthedocs.org/.
